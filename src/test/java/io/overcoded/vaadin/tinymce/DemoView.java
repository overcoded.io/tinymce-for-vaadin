package io.overcoded.vaadin.tinymce;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import io.overcoded.vaadin.tinymce.config.Skin;
import io.overcoded.vaadin.tinymce.config.TinyMceConfig;

import java.util.List;

@Route("")
public class DemoView extends VerticalLayout {
    public DemoView() {

        H1 title = new H1("TinyMCE for Vaadin");
        title.getStyle().set("font-size", "var(--lumo-font-size-l)").set("margin", "0");

        Button openButton = new Button("Open TinyMCE in dialog");
        openButton.addClickListener(event -> {
            Dialog dialog = new Dialog();
            dialog.setHeaderTitle("TinyMCE");
            Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> dialog.close());
            closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
            dialog.getHeader().add(closeButton);

            VerticalLayout dialogContent = new VerticalLayout();
            HorizontalLayout layout = new HorizontalLayout();
            TinyMceConfig config = TinyMceConfig.builder()
                    .width("100%")
                    .skin(Skin.OXIDE)
                    .menubar(List.of("false"))
                    .plugins(List.of("anchor", "lists", "link", "code", "fullscreen"))
                    .toolbar(List.of("undo", "redo", "|", "styles", "removeformat", "|", "bold", "italic", "underline", "|", "link", "anchor", "bullist", "numlist", "|", "alignleft", "aligncenter", "alignright", "alignjustify", "|", "outdent", "indent", "code", "fullscreen"))
                    .build();
            TinyMceField tinyMceField = new TinyMceField(config);
            tinyMceField.setContent("<h1>Hello World!</h1>");
            Button displayContentButton = new Button("Display content", (e) -> {
                layout.removeAll();
                layout.add(new Html("<div>" + tinyMceField.getContent() + "</div>"));
            });
            dialogContent.setWidthFull();
            tinyMceField.setWidthFull();

            dialogContent.add(tinyMceField);
            dialogContent.add(displayContentButton);
            dialogContent.add(layout);

            dialog.add(dialogContent);
            dialog.setSizeFull();
            dialog.setModal(true);
            dialog.setResizable(true);
            dialog.setDraggable(true);
            dialog.setCloseOnEsc(false);
            dialog.setCloseOnOutsideClick(false);
            dialog.open();
        });

        Button addButton = new Button("Add TinyMCE");
        addButton.addClickListener(event -> {
            TinyMceField tinyMceField = new TinyMceField("Description");
            tinyMceField.setWidthFull();
            tinyMceField.setContent("<p>Hello World!</p>");
            add(tinyMceField);
        });

        TinyMceField demoField = new TinyMceField("Content");
        Button showContent = new Button("Show content");
        showContent.addClickListener(event -> {
            Dialog dialog = new Dialog(new Html("<div>" + demoField.getContent() + "</div>"));
            dialog.open();
        });
        add(title, new HorizontalLayout(addButton, openButton), new Hr(), demoField, showContent);
    }
}
