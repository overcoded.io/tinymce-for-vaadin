window.setupEditor = function setupEditor(editor) {
    editor.on('init', (event) => {
        console.debug('The Editor has initialized.' + event.target);
        let style = document.createElement('style');
        style.innerHTML = ".tox-tinymce ~ textarea {display: none;} ";
        event.target.editorContainer.parentNode.appendChild(style);
    });
}

window.tinyMceChangeHandler = function tinyMceChangeHandler(event) {
    let id = event.target.editorContainer.parentNode.host.getAttribute("editor-id");
    let parent = document.getElementById(id);
    parent.setAttribute("content", event.target.getContent());
    parent.dispatchEvent(
        new CustomEvent("tinymce-update", {
            detail: {
                content: event.target.getContent()
            }
        })
    );
}