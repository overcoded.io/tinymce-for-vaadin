import {html, LitElement, TemplateResult} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import tinymce from 'tinymce';
import 'tinymce/themes/silver'
import 'tinymce/models/dom'
import 'tinymce/icons/default'
import 'tinymce/plugins/accordion'
import 'tinymce/plugins/advlist'
import 'tinymce/plugins/anchor'
import 'tinymce/plugins/autolink'
import 'tinymce/plugins/autoresize'
import 'tinymce/plugins/autosave'
import 'tinymce/plugins/charmap'
import 'tinymce/plugins/code'
import 'tinymce/plugins/codesample'
import 'tinymce/plugins/directionality'
import 'tinymce/plugins/emoticons'
import 'tinymce/plugins/fullscreen'
import 'tinymce/plugins/help'
import 'tinymce/plugins/image'
import 'tinymce/plugins/importcss'
import 'tinymce/plugins/insertdatetime'
import 'tinymce/plugins/link'
import 'tinymce/plugins/lists'
import 'tinymce/plugins/media'
import 'tinymce/plugins/nonbreaking'
import 'tinymce/plugins/pagebreak'
import 'tinymce/plugins/preview'
import 'tinymce/plugins/quickbars'
import 'tinymce/plugins/save'
import 'tinymce/plugins/searchreplace'
import 'tinymce/plugins/table'
import 'tinymce/plugins/visualblocks'
import 'tinymce/plugins/visualchars'
import 'tinymce/plugins/wordcount'

@customElement('tinymce-field')
export class TinyMceField extends LitElement {
    @property() editorId: string = "";
    @property() label: string = "";
    @property() config: any;
    @property() editorReadOnly: boolean = false;
    @property() content: string = "";

    constructor(editorId: string, config: any, editorReadOnly: boolean, content: string) {
        super();
        this.editorId = editorId;
        this.config = config;
        this.editorReadOnly = editorReadOnly;
        this.content = content;
    }

    firstUpdated() {
        console.debug("First updated is running.")
        this.initializeEditor();
    }

    initializeEditor() {
        console.debug("TinyMCE configuration from backend: ", this.config);
        const config = this.prepareTinyMceConfig(this.config);
        console.debug("Mapped TinyMCE configuration: ", config);

        setTimeout(() => {
            tinymce.init({
                target: this.shadowRoot.querySelector(`#tinymce-${this.editorId}`),
                ...config,
                readonly: this.editorReadOnly,
                setup: (editor) => {
                    editor.on('init', () => {
                        console.debug("Initializing editor", editor);
                        console.debug("Initializing with content", this.content);
                        editor.setContent(this.content || '');
                    });
                    editor.on('change', () => {
                        console.debug("Change event has been triggered.")
                        this.dispatchEvent(new CustomEvent('tinymce-update', {
                            detail: {
                                content: editor.getContent()
                            }
                        }));
                    });
                }
            });
        }, 100);
    }

    prepareTinyMceConfig(config: any): any {
        const configMapping: { [key: string]: string } = {
            apiKey: 'license_key',
            setup: 'setup',
            toolbar: 'toolbar',
            menubar: 'menubar',
            plugins: 'plugins',
            contentCss: 'content_css',
            contentStyle: 'content_style',
            width: 'width',
            height: 'height',
            toolbarMode: 'toolbar_mode',
            contextMenu: 'contextmenu',
            powerPasteWordImport: 'powerpaste_word_import',
            powerPasteHtmlImport: 'powerpaste_html_import',
            powerPasteAllowLocalImages: 'powerpaste_allow_local_images',
            resize: 'resize',
            skin: 'skin',
            icons: 'icons',
            iconsUrl: 'icons_url',
            entityEncoding: 'entity_encoding',
            onChange: 'on-Change',
        };
        const lowercaseKeys = new Set([
            'toolbarMode',
            'powerPasteWordImport',
            'powerPasteHtmlImport',
            'resize',
            'skin',
            'icons'
        ]);

        const filteredConfig: any = {};

        for (const [key, value] of Object.entries(config)) {
            if (value !== undefined && value !== null) {
                const tinyMceKey = configMapping[key] || key;

                if (Array.isArray(value) && value.length > 0) {
                    filteredConfig[tinyMceKey] = value.join(' ');
                } else if (typeof value === 'string' && lowercaseKeys.has(key)) {
                    filteredConfig[tinyMceKey] = value.toLowerCase();
                } else {
                    filteredConfig[tinyMceKey] = value;
                }
            }
        }

        return filteredConfig;
    }

    render(): TemplateResult {
        return html`
            <vaadin-custom-field style="width: 100%;">
                <label slot="label"
                       class="tinymce-label"
                       id="label-${this.editorId}"
                       for="tinymce-${this.editorId}">
                    ${this.label}
                </label>
                <div style="margin-top: var(--lumo-space-xs);">
                    <textarea id="tinymce-${this.editorId}"></textarea>
                </div>
            </vaadin-custom-field>
        `;
    }

    update(changedProperties: Map<string, any>) {
        if (changedProperties.has("editorReadOnly")) {
            let editor = tinymce.get(this.editorId);
            if (editor) {
                editor.mode.set(this.editorReadOnly ? "readonly" : "design");
            }
        }
        super.update(changedProperties);
    }
}