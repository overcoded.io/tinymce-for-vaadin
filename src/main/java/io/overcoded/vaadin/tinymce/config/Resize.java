package io.overcoded.vaadin.tinymce.config;

/**
 * The resize attribute gives you the ability to disable the resize handle or set it
 * to resize the editor both horizontal and vertically.
 */
public enum Resize {
    /**
     * The editor will resize vertically
     */
    TRUE,
    /**
     * To remove the resize handle and disable resizing
     */
    FALSE,
    /**
     * To allow the user to resize the editor both horizontally and vertically
     */
    BOTH
}
