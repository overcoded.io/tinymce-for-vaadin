package io.overcoded.vaadin.tinymce.config;

/**
 * This option controls how content pasted from Microsoft Word is filtered.
 */
public enum PowerPasteImport {
    /**
     * Preserve the structure of the content such as headings, tables, and lists but remove inline
     * styles and classes. This results in simple content that uses the site’s CSS stylesheet while
     * retaining the semantic structure from the original document.
     */
    CLEAN,
    /**
     * Preserve the inline formatting and structure of the original document. Invalid and proprietary
     * styles, tags and attributes are still removed ensuring that the HTML is valid while more closely
     * matching the original document formatting.
     */
    MERGE,
    /**
     * Prompt the user to choose between the clean and merge options after attempting to paste HTML content.
     */
    PROMPT
}
