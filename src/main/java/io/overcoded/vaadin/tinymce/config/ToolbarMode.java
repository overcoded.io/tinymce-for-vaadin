package io.overcoded.vaadin.tinymce.config;

/**
 * To control the behavior of the toolbar
 */
public enum ToolbarMode {
    /**
     * If the toolbar_mode is configured to floating, the toolbar appears under the toolbar
     * overflow icon in a floating shelf format when the toolbar overflow icon Drawer is clicked.
     */
    FLOATING,
    /**
     * If the toolbar_mode is configured to sliding, the toolbar appears as a fixed toolbar under
     * the first toolbar when the toolbar overflow icon Drawer is clicked.
     */
    SLIDING,
    /**
     * The scrolling toolbar mode is intended for touch screen devices.
     */
    SCROLLING,
    /**
     * If the toolbar_mode is configured to wrap, the overflow toolbar buttons will be shown on
     * one or more toolbars below the primary toolbar.
     */
    WRAP
}
