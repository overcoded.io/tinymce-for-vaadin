package io.overcoded.vaadin.tinymce.config;

/**
 * The icons option allows the editor icons to be extended or replaced using an icon pack.
 * For information visit: <a href="https://www.tiny.cloud/docs/configure/editor-appearance/#icons">...</a>
 */
public enum Icon {
    /**
     * Bootstrap icons
     */
    BOOTSTRAP,
    /**
     * Material icons
     */
    MATERIAL,
    /**
     * Small icons
     */
    SMALL,
    /**
     * Jam icons
     */
    JAM,
    /**
     * Thin icons
     */
    THIN;
}
