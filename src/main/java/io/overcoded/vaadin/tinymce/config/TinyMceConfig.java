package io.overcoded.vaadin.tinymce.config;

import lombok.Builder;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * Configuration class for TinyMCE
 */
@Data
@Builder
public class TinyMceConfig implements Serializable {
    @Serial
    private static final long serialVersionUID = 1905122041950251207L;

    /**
     * To use TinyMCE from the Tiny Cloud, add the api-key attribute to the tinymce-editor element with an API from Tiny Account.
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/my-account">...</a>
     */
    @Builder.Default
    private String apiKey = "gpl";

    /**
     * This option controls how entities/characters get processed by TinyMCE.
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/tinymce/6/content-filtering/#entity_encoding">...</a>
     */
    @Builder.Default
    private String entityEncoding = "raw";

    /**
     * Name of setup method which should bind events for the TinyMCE Web Component.
     */
    @Builder.Default
    private String setup = "setupEditor";

    /**
     * List of plugins which should be loaded.
     * The following example:
     * <code>
     *  List.of("advlist", "autolink", "link", "image", "lists", "charmap", "print", "preview")
     * </code>
     * Will produce something like this:
     * <code>
     *     {@code <tinymce-editor plugins="advlist autolink link image lists charmap print preview"></tinymce-editor>}
     * </code>
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#loadingplugins">...</a>
     */
    private List<String> plugins;

    /**
     * To set the width of the editor (content area and user interface)
     * For more information, please visit:
     *  - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingtheeditorwidth">...</a>
     */
    private String width;

    /**
     * To set the height of the editor (content area and user interface)
     * For more information, please visit:
     *  - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingtheeditorheight">...</a>
     */
    private String height;

    /**
     * List of toolbar buttons, which should available in the editor.
     * If you want to group buttons, you can add `|` as a toolbar entry.
     * The following example:
     * <code>
     *  List.of("undo", "redo", "|", "styleselect", "|", "bold", "italic", "|", "alignleft", "aligncenter", "alignright", "alignjustify", "|", "outdent", "indent")
     * </code>
     * Will produce something like this:
     * <code>
     *     {@code <tinymce-editor toolbar="undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent"></tinymce-editor>}
     * </code>
     * If you want to disable the toolbar, you need to set it to false with: List.of("false").
     * By default toolbar is disabled
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingthetoolbar">...</a>
     */
    private List<String> toolbar;

    /**
     * Set the toolbar mode for the editor.
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingthetoolbarmode">...</a>
     */
    private ToolbarMode toolbarMode;

    /**
     * List of menubar items, which should available on the menubar.
     * The following example:
     * <code>
     *  List.of("file", "edit", "insert", "view", "format", "table", "tools", "help")
     * </code>
     * Will produce something like this:
     * <code>
     *     {@code <tinymce-editor menubar="file edit insert view format table tools help"></tinymce-editor>}
     * </code>
     * If you want to disable the menubar, you need to set it to false with: List.of("false").
     * By default menubar is disabled
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingthemenubar">...</a>
     */
    private List<String> menubar;

    /**
     * To change the context menu sections that can be shown in the editor context menu
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingcontextmenu">...</a>
     */
    private List<String> contextMenu;

    /**
     * To set the CSS for the content area of the editor.
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingcontentstylesheets">...</a>
     */
    private String contentCss;

    /**
     * To apply a small set of CSS styles to the editor, use the content_style attribute.
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingcontentstyling">...</a>
     */
    private String contentStyle;

    /**
     * To control how content pasted from Microsoft Word is filtered.
     * This setting only applies if the PowerPaste plugin (powerpaste) is enabled.
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingpowerpasteswordimportmethod">...</a>
     */
    private PowerPasteImport powerPasteWordImport;

    /**
     * To control how content pasted from sources other than Microsoft Word is filtered.
     * This setting only applies if the PowerPaste plugin (powerpaste) is enabled.
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingpowerpasteshtmlimportmethod">...</a>
     */
    private PowerPasteImport powerPasteHtmlImport;

    /**
     * To prevent Base64 encoded images with a data URI from being pasted into the editor, set this to false.
     * This setting only applies if the PowerPaste plugin (powerpaste) is enabled.
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingpowerpastetoallowlocalimages">...</a>
     */
    private Boolean powerPageAllowLocalImages;

    /**
     * The resize attribute gives you the ability to disable the resize handle or set it to resize the editor both horizontal and vertically.
     * By default, the editor will resize vertically (resize="true").
     * To allow the user to resize the editor both horizontally and vertically, set this to "BOTH".
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#showingresizehandles">...</a>
     */
    private Resize resize;

    /**
     * To apply a custom skin to the editor change this attribute.
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingtheeditorskin">...</a>
     */
    private Skin skin;

    /**
     * To apply a bundled set of custom or premium icons to the editor, use the icons attribute.
     * <p>
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingtheiconpack">...</a>
     */
    private Icon icon;

    /**
     * To apply a hosted set of custom or premium icons to the editor, use the icons_url attribute.
     * For more information, please visit:
     * - <a href="https://www.tiny.cloud/docs/integrations/webcomponent/#settingtheiconpackurl">...</a>
     */
    private String iconUrl;

    /**
     * Method which should be called on change
     */
    private String onChange;
}
