package io.overcoded.vaadin.tinymce.config;

/**
 * This option allows you to specify the skin that TinyMCE should use
 */
public enum Skin {
    /**
     * TinyMCE's default skin
     */
    OXIDE,

    /**
     * TinyMCE's material_classic skin
     */
    MATERIAL_CLASSIC,
    /**
     * TinyMCE's material_outline skin
     */
    MATERIAL_OUTLINE,
    /**
     * TinyMCE's bootstrap skin
     */
    BOOTSTRAP,
    /**
     * TinyMCE's fabric skin
     */
    FABRIC,
    /**
     * TinyMCE's fluent skin
     */
    FLUENT,
    /**
     * TinyMCE's borderless skin
     */
    BORDERLESS,
    /**
     * TinyMCE's small skin
     */
    SMALL,
    /**
     * TinyMCE's jam skin
     */
    JAM,
    /**
     * TinyMCE's naked skin
     */
    NAKED,
    /**
     * TinyMCE's outside skin
     */
    OUTSIDE,
    /**
     * TinyMCE's snow skin
     */
    SNOW,
    /**
     * TinyMCE's custom skin
     */
    CUSTOM
}
