package io.overcoded.vaadin.tinymce;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;
import io.overcoded.vaadin.tinymce.config.TinyMceConfig;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.UUID;

/**
 * CustomField which should contain the TinyMCE rich-text editor
 */
@Slf4j
@Getter
@Tag("tinymce-field")
@JsModule("./@overcoded/tinymce/tinymce-field.ts")
@JsModule("./@overcoded/tinymce/tinymce-handlers.js")
@JsModule("tinymce/tinymce.js")
@JsModule("tinymce/skins/content/default/content.js")
@JsModule("tinymce/skins/content/dark/content.js")
@JsModule("tinymce/skins/ui/oxide/content.js")
@JsModule("tinymce/skins/ui/oxide/skin.js")
@JsModule("tinymce/skins/ui/oxide/skin.shadowdom.js")
@JsModule("tinymce/skins/ui/oxide-dark/content.js")
@JsModule("tinymce/skins/ui/oxide-dark/skin.js")
@JsModule("tinymce/skins/ui/oxide-dark/skin.shadowdom.js")
@NpmPackage(value = "tinymce", version = "7.1.1")
public class TinyMceField extends CustomField<String> {
    /**
     * Configuration of TinyMCE
     */
    private TinyMceConfig config;

    /**
     * Label of the input field
     */
    private String label;
    /**
     * Identifier of the field, label and generated editor
     */
    private String editorId;
    /**
     * Content of the rich-text editor
     */
    private String content;
    /**
     * Backend state of editor read-only option
     */
    private boolean editorReadOnly;

    public TinyMceField() {
        this(TinyMceConfig.builder().build());
    }

    public TinyMceField(String label) {
        this(label, TinyMceConfig.builder().build());
    }

    public TinyMceField(TinyMceConfig config) {
        this(null, config);
    }

    public TinyMceField(String label, TinyMceConfig config) {
        addListener(TinyMceUpdateEvent.class, this::updateContent);
        String generatedId = UUID.randomUUID().toString();
        setConfig(config);
        setId(generatedId);
        setEditorReadOnly(false);
        setEditorId(generatedId);
        if (Objects.nonNull(label)) {
            setLabel(label);
        }
    }

    @Override
    protected String generateModelValue() {
        return getContent();
    }

    @Override
    protected void setPresentationValue(String s) {
        setContent(s);
    }

    public void setEditorId(String editorId) {
        this.editorId = editorId;
        getElement().setProperty("editorId", editorId);
    }

    public void setConfig(TinyMceConfig config) {
        this.config = config;
        getElement().setPropertyBean("config", config);
    }

    public void setContent(String content) {
        this.content = content;
        super.setValue(content);
        getElement().setProperty("content", content);
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
        super.setLabel(label);
        getElement().setProperty("label", label);
    }

    @Override
    public String getValue() {
        return getContent();
    }

    @Override
    public void setValue(String content) {
        setContent(content);
    }

    public void setEditorReadOnly(boolean editorReadOnly) {
        this.editorReadOnly = editorReadOnly;
        super.setReadOnly(editorReadOnly);
        getElement().setProperty("editorReadOnly", editorReadOnly);
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        setEditorReadOnly(readOnly);
    }


    private void updateContent(TinyMceUpdateEvent event) {
        setContent(event.getContent());
    }
}
