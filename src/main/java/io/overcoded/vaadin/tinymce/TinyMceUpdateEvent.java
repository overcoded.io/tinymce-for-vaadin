package io.overcoded.vaadin.tinymce;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import lombok.Getter;

/**
 * Update event to notify backend about content change
 */
@DomEvent("tinymce-update")
public class TinyMceUpdateEvent extends ComponentEvent<TinyMceField> {
    /**
     * Updated (new) content
     */
    @Getter
    private final String content;

    /**
     * Creates a new event using the given source and indicator whether the
     * event originated from the client side or the server side.
     *
     * @param source     the source component
     * @param fromClient <code>true</code> if the event originated from the client
     *                   side, <code>false</code> otherwise
     * @param content    the new content, which should be saved
     */
    public TinyMceUpdateEvent(TinyMceField source,
                              boolean fromClient,
                              @EventData("event.detail.content") String content) {
        super(source, fromClient);
        this.content = content;
    }

}
