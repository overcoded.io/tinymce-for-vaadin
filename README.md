# tinymce-for-vaadin
Another [TinyMCE](https://www.tiny.cloud/) wrapper project for Vaadin. This project is wrapping the [official TinyMCE Web Component](https://github.com/tinymce/tinymce-webcomponent) as a [CustomField](https://vaadin.com/docs/latest/components/custom-field).

![TinyMCE for Vaadin: Preview](preview.png)

## Configuration
This component can be configured through `TinyMceConfig` class, where you can configure:

- [api key](https://www.tiny.cloud/my-account)
- [plugins](https://www.tiny.cloud/docs/integrations/webcomponent/#loadingplugins) 
- [toolbar](https://www.tiny.cloud/docs/integrations/webcomponent/#settingthetoolbar)
- [toolbar mode](https://www.tiny.cloud/docs/integrations/webcomponent/#settingthetoolbarmode)
- [menubar](https://www.tiny.cloud/docs/integrations/webcomponent/#settingthemenubar)
- [context menu](https://www.tiny.cloud/docs/integrations/webcomponent/#settingcontextmenu)
- [content css](https://www.tiny.cloud/docs/integrations/webcomponent/#settingcontentstylesheets)
- [content style](https://www.tiny.cloud/docs/integrations/webcomponent/#settingcontentstyling)
- [width](https://www.tiny.cloud/docs/integrations/webcomponent/#settingtheeditorwidth)
- [height](https://www.tiny.cloud/docs/integrations/webcomponent/#settingtheeditorheight)
- [resize](https://www.tiny.cloud/docs/integrations/webcomponent/#showingresizehandles)
- [skin](https://www.tiny.cloud/docs/integrations/webcomponent/#settingtheeditorskin)
- [icon](https://www.tiny.cloud/docs/integrations/webcomponent/#settingtheiconpack)

## License
This project is licensed under the terms of the GNU General Public License v3.0. See the LICENSE file for details.

## Using TinyMCE
This project includes TinyMCE, which is licensed under the GPLv3.